# Creating new user name and password for Parse Dashboard Login.
			echo "############### IMPORTANT  #######################"
			echo "--- Please create your new User name and Password "
			echo "--------------------------------------------------"
			echo ""
			echo "Parse User Name (Case sensative):  "
			read user
			echo "Parse Password (Case sensative):  "
			read pass
			sleep 1
			sed 's/masterid/'"$NEW_ID_MASTER"'/g; s/appid/'"$NEW_ID_CLIENT"'/g; s/domain/'"$input"'/g; s/user-sample/'"$user"'/g; s/pass-sample/'"$pass"'/g' /root/parse-full-server-setup-digitalocean/parse-dashboard-config.json > /root/parse-dashboard/Parse-Dashboard/parse-dashboard-config.json

			# Embed new Generated ID's to Index.js file for Parse Server
			sed 's/masterid/'"$NEW_ID_MASTER"'/g; s/appid/'"$NEW_ID_CLIENT"'/g; s/domain/'"$input"'/g' /root/parse-full-server-setup-digitalocean/parse_app_setup.js > /root/parse-server-example/index.js

			echo "- Creating First MongoDb Entry -"
			sleep 2
			curl -X POST \
				-H "X-Parse-Application-Id: $NEW_ID_CLIENT" \
				-H "Content-Type: application/json" \
				-d '{"score":1337,"playerName":"Sammy","cheatMode":false}' \
				https://localhost:1337/parse/classes/GameScore

			echo "------------------------------------------------------------------"
			echo "############## IMPORTANT - WRITE THIS DOWN  ######################"
			echo " ---- MASTER KEY (Keep this private): $NEW_ID_MASTER"
			echo " ---- CLIENT KEY: $NEW_ID_CLIENT"
			echo ""
			echo " ----------------------- LOCATIONS -------------------------------"
			echo " - Parse Dashboard: http://$input:4040 - Sorry still can't get HTTPS to work"
			echo " - Parse Server: https://$input/parse"
			echo " - Parse LiveQuery Server: ws://$input:1337"
			echo ""
			echo "################ GOOD LUCK BUILDING STUFF  ######################"
			echo "------------------------------------------------------------------"

			sleep 5

			echo -p "Do you have everything you need to start? (y/n)?"
				read restart_this

				case $restart_this in
					y)
						echo "Rebooting now";
						reboot
						;;
					n)
						echo "- Some services might not work if you don't restart the server -"
						;;
				esac

			;;
		n)
			echo "------------------------------------------------------------------"
			echo "- You can restart this script once you have everything prepared. -"
			echo "------------------------------------------------------------------"
			;;
	esac